<html>
<head>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<style>
li{
    list-style-type: none;
}
.child_list{
    display:none;
}
.super_child_list{
    display:none;
}
.parent_minus_icon{
    display:none;
}
.child_minus_icon{
    display:none;
}
li{cursor: context-menu;}
</style>
</head>
<body>
    <h1>Tree View</h1>

    <?php
    $nested = array(
        "parent1"=>array(
            "chiled1"=>array(
                "ssuper_child1",
                "ssuper_child1",
                "ssuper_child1",
            ),
            "chiled2"=>array(
                "ssuper_child21",
                "ssuper_child22",
                "ssuper_child23",
            ),
            "chiled3"=>array(
                "ssuper_child31",
                "ssuper_child32",
                "ssuper_child33",
            )
        ),
        "parent2"=>array(
            "chiled1"=>array(
                "ssuper_child1",
                "ssuper_child1",
                "ssuper_child1",
            ),
            "chiled2"=>array(
                "ssuper_child21",
                "ssuper_child22",
                "ssuper_child23",
            ),
            "chiled3"=>array(
                "ssuper_child31",
                "ssuper_child32",
                "ssuper_child33",
            )
        ),
        "parent3"=>array(
            "chiled1"=>array(
                "ssuper_child1",
                "ssuper_child1",
                "ssuper_child1",
            ),
            "chiled2"=>array(
                "ssuper_child21",
                "ssuper_child22",
                "ssuper_child23",
            ),
            "chiled3"=>array(
                "ssuper_child31",
                "ssuper_child32",
                "ssuper_child33",
            )
        ),
    );

    // echo "<pre>";
    // print_r($nested);
    // exit();
    ?>

<?php $i=1;$y=1; foreach($nested as $key=>$parent){ ?>

    
    <li style="width: 100px;" id="parent_list_<?php echo $i;?>" onclick="show_child(this);"><span id="parent_plus_icon_<?php echo $i;?>">+</span><span id="parent_minus_icon_<?php echo $i;?>" class="parent_minus_icon">-</span>
        <?php echo($key); ?> 
    </li>
            <?php foreach($parent as $key=>$child){ ?>
                    <li style="margin-left: 20px;width: 100px;" onclick="show_super_child(this);" class="child_list_<?php echo $i;?> child_list" id="child_list_<?php echo $i;?>_<?php echo $y;?>" onclick="show_child(this);"><span id="child_plus_icon_<?php echo $y;?>">+</span><span id="child_minus_icon_<?php echo $y;?>" class="child_minus_icon">-</span>
                    <?php echo($key); ?>
                    </li>
                            <?php foreach($child as $key=>$super_child){ ?>
                                    <li style="margin-left: 40px;" class="super_child_list_<?php echo $y;?> super_child_list">- <?php echo($super_child); ?></li>
                            <?php } ?>
                   
                
            <?php $y++;} ?>
        
    

<?php $i++;} ?>
<script>
function show_child(elem) {
    var id = $(elem).attr("id");
    var id_number = id.split("_")
    var class_name = ".child_list_"+id_number[2];
    var icon_plus_name = "#parent_plus_icon_"+id_number[2];
    var icon_minus_name = "#parent_minus_icon_"+id_number[2];
    if($(class_name).is(":visible")){
        $(class_name).hide();

        $(icon_plus_name).show();
        $(icon_minus_name).hide();
    }else{
        $(class_name).show();
       
        $(icon_plus_name).hide();
        $(icon_minus_name).show();
    }

   
}

function show_super_child(elem) {
    var id = $(elem).attr("id");
    var id_number = id.split("_")
    var class_name = ".super_child_list_"+id_number[3];
    var icon_plus_name = "#child_plus_icon_"+id_number[3];
    var icon_minus_name = "#child_minus_icon_"+id_number[3];
    if($(class_name).is(":visible")){
        $(class_name).hide();

        $(icon_plus_name).show();
        $(icon_minus_name).hide();
    }else{
        $(class_name).show();
               
        $(icon_plus_name).hide();
        $(icon_minus_name).show();
    }
   
}
</script>
</body>
</html>